 <head>
  <meta charset="UTF-8">
</head> 

<h1>Laboratório de Técnicas de programação 1</h1>
<p><strong>Instituição:</strong> Universidade de Brasília;<br>
<p><strong>Aluno:</strong> Antônio Henrique de Moura Rodrigues;<br>
<p><strong>Curso:</strong> Ciência da Computação;<br>
<strong>Matrícula:</strong> 15 / 0118236;<br>
<strong>Professor:</strong> Teófilo de Campos - Turma <strong>A</strong>.</p>

<h2><u>1.1 Classe Bola </h2></u><br>

<h2>1.1 Introdução</h2>

<p>Relatório referente a primeira parte do laboratório de Técnicas de programação 1, orientado pelo professor Teófilo de Campos<br>
<strong>Objetivo:</strong> Se familiarizar com a programação orientada a objeto, praticar conceitos relacionados a classes, atributos, métodos, abstração, escapsulamento, herança e polimorfismo; e<br>
Criar funções que auxiliam na abstração do percurso de uma bola, tendo como retorno coordenadas que foram tratadas por softwares matemáticos afim de avaliar seu comportamento </p>

<h2>1.2 Requisitos</h2>

<p><strong>Pacotes:</strong><br>
<strong>Bibliotecas:</strong><br>
    <em>simulation:</em> Biblioteca que contém a classe abstrata simulation;<br>
    <em>iostream:</em> Biblioteca que auxilia nos prints;<br>
    <em>ball:</em> Biblioteca que contém a classe bola.<br>
<strong>Compiladores:</strong> g++ (GCC) 2015 Free Software Foundation, Inc.<br>
<strong>Versão C++:</strong> 5.3.0</p>

<h2>1.3 Linha de comando</h2>

<p>As linhas usadas para compilar os arquivos foram:<br>
<blockquote>
<pre><code><p> cd < PASTA DO ARQUIVO > <br>
g++ ball.cpp ball.h test-ball.cpp </p></pre></code>
</blockquote>

<h2>1.4 Arquivos</h2>

<p><strong>ball.h:</strong>Arquivo cabeçalho. Aqui se encontra todos os atributos e todos os métodos que da classe bola que serão utilizados quando forem criados os objetos<br>
    <em>Atributos da bola</em>: Posição e velocidade no eixo x e y, raio e massa, gravidade aplicada e as dimensões da caixa onde a bola se encontra; <br>
    <em>Métodos da bola: Step: Calcula o passo da bola. Display: Imprime os valores na tela.</em><br>
<strong>ball.cpp:</strong> Arquivo de código. Aqui fica as implementações dos métodos citados no arquivo cabeçalho<br>
<strong>test-ball.cpp:</strong> Aqui é criado o objeto, utilizando a classe criada ball e chamadas de métodos visando descobrir onde se encontra a bola.</p>

<h2>1.5 Diagrama das classes</h2>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/classdiagram.png">

<h2>1.6 Saídas Geradas</h2>

<strong>Coordenadas:</strong> <br>
0.01 -0.00877778<br>
0.02 -0.0284444<br>
0.03 -0.059<br>
0.04 -0.100444<br>
0.05 -0.152778<br>
0.06 -0.216<br>
0.07 -0.290111<br>
0.08 -0.375111<br>
0.09 -0.471<br>
0.1  -0.577778<br>
0.11 -0.695444<br>
0.12 -0.824<br>
0.13 -0.824<br>
0.14 -0.695444<br>
0.15 -0.577778<br>
0.16 -0.471<br>
0.17 -0.375111<br>
0.18 -0.290111<br>
0.19 -0.216<br>
0.2  -0.152778<br>
0.21 -0.100444<br>
0.22 -0.059<br>
0.23 -0.0284444<br>
0.24 -0.00877778<br>
0.25  1.1534e-016<br>
0.26 -0.00211111<br>
0.27 -0.0151111<br>
0.28 -0.039<br>
0.29 -0.0737778<br>
0.3  -0.119444<br>
0.31 -0.176<br>
0.32 -0.243444<br>
0.33 -0.321778<br>
0.34 -0.411<br>
0.35 -0.511111<br>
0.36 -0.622111<br>
0.37 -0.744<br>
0.38 -0.876778<br>
0.39 -0.876778<br>
0.4  -0.744<br>
0.41 -0.622111<br>
0.42 -0.511111<br>
0.43 -0.411<br>
0.44 -0.321778<br>
0.45 -0.243444<br>
0.46 -0.176<br>
0.47 -0.119444<br>
0.48 -0.0737778<br>
0.49 -0.039<br>
0.5  -0.0151111<br>
0.51 -0.00211111<br>
0.52  2.0957e-016<br>
0.53 -0.00877778<br>
0.54 -0.0284444<br>
0.55 -0.059<br>
0.56 -0.100444<br>
0.57 -0.152778<br>
0.58 -0.216<br>
0.59 -0.290111<br>
0.6  -0.375111<br>
0.61 -0.471<br>
0.62 -0.577778<br>
0.63 -0.695444<br>
0.64 -0.824<br>
0.65 -0.824<br>
0.66 -0.695444<br>
0.67 -0.577778<br>
0.68 -0.471<br>
0.69 -0.375111<br>
0.7  -0.290111<br>
0.71 -0.216<br>
0.72 -0.152778<br>
0.73 -0.100444<br>
0.74 -0.059<br>
0.75 -0.0284444<br>
0.76 -0.00877778<br>
0.77  3.37385e-016<br>
0.78 -0.00211111<br>
0.79 -0.0151111<br>
0.8  -0.039<br>
0.81 -0.0737778<br>
0.82 -0.119444<br>
0.83 -0.176<br>
0.84 -0.243444<br>
0.85 -0.321778<br>
0.86 -0.411<br>
0.87 -0.511111<br>
0.88 -0.622111<br>
0.89 -0.744<br>
0.89 -0.876778<br>
0.88 -0.876778<br>
0.87 -0.744<br>
0.86 -0.622111<br>
0.85 -0.511111<br>
0.84 -0.411<br>
0.83 -0.321778<br>
0.82 -0.243444<br>
0.81 -0.176<br>
0.8  -0.119444<br>
0.79 -0.0737778</p>
<p> (resultado "default", com os valores prontos do código, sendo eles dt = 0.1/30 e loop até 100).</p>


<p>Foi printado as coordenadas x e y, respectivamente, esperadas do curso da bolinha, sendo o valor inicial próximo ao (x,y) = (0,0), onde se encontra a origem da bolinha.<br>
Ela nunca passa do eixo X, por isso os valores de y sempre são negativos. <br>
Quando x encontra um valor máximo, ele começa a diminuir, isso significa que x encontrou uma parede, bateu e voltou.</p>


<strong>Gráfico:</strong><br>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/1000Loop10000.png" alt="Gráfico gerado no software Octave, valores das variáveis informados na imagem." />
<p>Os resultados obtidos são marcados no par de eixos para avaliar com mais eficácia o trajeto da bola.<br>
*Gráfico gerado no software Octave, valores das variáveis informados na imagem.</p>










<h2><u> 2. Classe Spring </h2></u>

<h2>2.1 Introdução</h2>

<p>Relatório referente a segunda parte do laboratório de Técnicas de programação 1, orientado pelo professor Teófilo de Campos<br>
<strong>Objetivo:</strong> Se familiarizar com a programação orientada a objeto, praticar conceitos relacionados a classes, atributos, métodos, abstração, escapsulamento, herança e polimorfismo; e<br>
Criar funções que auxiliam na abstração do percurso de uma mola, onde essa possui duas massas nas suas pontas. O programa retorna coordenadas que foram tratadas por softwares matemáticos afim de avaliar seu comportamento. </p>

<h2>2.2 Requisitos</h2>

<p><strong>Pacotes:</strong><br>
<strong>Bibliotecas:</strong><br>
    <em>simulation:</em> Biblioteca que contém a classe abstrata simulation;<br>
    <em>iostream:</em> Biblioteca que auxilia nos prints;<br>
    <em>springmass:</em> Biblioteca que contém a classe mola e massa.<br>
<strong>Compiladores:</strong> g++ (GCC) 2015 Free Software Foundation, Inc.<br>
<strong>Versão C++:</strong> 5.3.0</p>

<h2>2.3 Linha de comando</h2>

<p>As linhas usadas para compilar os arquivos foram:<br>
<blockquote>
<pre><code><p> cd < PASTA DO ARQUIVO > <br>
g++ springmass.cpp test-springmass.cpp </p></pre></code>
</blockquote>

<h2>2.4 Arquivos</h2>

<p><strong>springmass.h:</strong>Arquivo cabeçalho. Aqui se encontra todos os atributos e todos os métodos que da classe spring que serão utilizados quando forem criados os objetos<br>
    <em>Atributos da mola</em>: Massa adicionadas as duas pontas, sua força, energia aplicada, rigidez, amortecimento e comprimento; <br>
    <em>Métodos da mola: Step: Calcula o passo da mola. Display: Imprime os valores na tela.</em><br>
<strong>springmass.cpp:</strong> Arquivo de código. Aqui fica as implementações dos métodos citados no arquivo cabeçalho<br>
<strong>test-springmass.cpp:</strong> Aqui é criado o objeto, utilizando a classe criada springmass e chamadas de métodos visando descobrir onde se encontra a mola e suas massas</p>

<h2>2.5 Diagrama das classes</h2>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/classdiagram2.png">

<h2>2.6 Saídas Geradas</h2>

<p><strong>Coordenadas:</strong> <br>
energia total: 0.175914<br>
X1: -0.5 Y1: 0.104953<br>
X2: 0.5 Y2: 0.0991<br>
energia total: 0.310829<br>
X1: -0.499951 Y1: 0.938807<br>
X2: 0.500146 Y2: 0.928932<br>
energia total: 0.311291<br>
X1: -0.499902 Y1: 0.938807<br>
X2: 0.500391 Y2: 0.928932<br>
energia total: 0.310704<br>
X1: -0.499815 Y1: 0.938807<br>
X2: 0.500847 Y2: 0.928932<br>
energia total: 0.311328<br>
X1: -0.499728 Y1: 0.938807<br>
X2: 0.501479 Y2: 0.928932<br>
energia total: 0.310754<br>
X1: -0.499639 Y1: 0.938807<br>
X2: 0.502288 Y2: 0.928932<br>
energia total: 0.311395<br>
X1: -0.49955 Y1: 0.938807<br>
X2: 0.503279 Y2: 0.928932<br>
energia total: 0.310837<br>
X1: -0.499458 Y1: 0.938807<br>
X2: 0.504453 Y2: 0.928932<br>
energia total: 0.311497<br>
X1: -0.499365 Y1: 0.938807<br>
X2: 0.505816 Y2: 0.928932<br>
energia total: 0.310955<br>
X1: -0.49927 Y1: 0.938807<br>
X2: 0.507372 Y2: 0.928932<br>
energia total: 0.311639<br>
X1: -0.499172 Y1: 0.938807<br>
X2: 0.509126 Y2: 0.928932<br>
energia total: 0.311118<br>
X1: -0.499071 Y1: 0.938807<br>
X2: 0.511085 Y2: 0.928932<br>
energia total: 0.311828<br>
X1: -0.498967 Y1: 0.938807<br>
X2: 0.513255 Y2: 0.928932<br>
energia total: 0.311332<br>
X1: -0.498859 Y1: 0.938807<br>
X2: 0.515644 Y2: 0.928932<br>
energia total: 0.312076<br>
X1: -0.498748 Y1: 0.938807<br>
X2: 0.51826 Y2: 0.928932<br>
energia total: 0.311611<br>
X1: -0.498632 Y1: 0.938807<br>
X2: 0.521113 Y2: 0.928932<br>
energia total: 0.312395<br>
X1: -0.498511 Y1: 0.938807<br>
X2: 0.524212 Y2: 0.928932<br>
energia total: 0.31197<br>
X1: -0.498385 Y1: 0.938807<br>
X2: 0.527568 Y2: 0.928932<br>
energia total: 0.312804<br>
X1: -0.498253 Y1: 0.938807<br>
X2: 0.531193 Y2: 0.928932<br>
energia total: 0.31243<br>
X1: -0.498115 Y1: 0.938807<br>
X2: 0.5351 Y2: 0.928932<br>
energia total: 0.313325<br>
X1: -0.497971 Y1: 0.938807<br>
X2: 0.539303 Y2: 0.928932<br>
energia total: 0.313015<br>
X1: -0.497819 Y1: 0.938807<br>
X2: 0.543816 Y2: 0.928932<br>
energia total: 0.313988<br>
X1: -0.49766 Y1: 0.938807<br>
X2: 0.548656 Y2: 0.928932<br>
energia total: 0.313759<br>
X1: -0.497492 Y1: 0.938807<br>
X2: 0.553839 Y2: 0.928932<br>
energia total: 0.31483<br>
X1: -0.497316 Y1: 0.938807<br>
X2: 0.559384 Y2: 0.928932<br>
energia total: 0.314704<br>
X1: -0.49713 Y1: 0.938807<br>
X2: 0.565311 Y2: 0.928932<br>
energia total: 0.315899<br>
X1: -0.496933 Y1: 0.938807<br>
X2: 0.571642 Y2: 0.928932<br>
energia total: 0.315904<br>
X1: -0.496726 Y1: 0.938807<br>
X2: 0.578398 Y2: 0.928932<br>
energia total: 0.317256<br>
X1: -0.496506 Y1: 0.938807<br>
X2: 0.585605 Y2: 0.928932<br>
energia total: 0.317429<br>
X1: -0.496274 Y1: 0.938807<br>
X2: 0.593289 Y2: 0.928932<br>
energia total: 0.318979<br>
X1: -0.496028 Y1: 0.938807<br>
X2: 0.601478 Y2: 0.928932<br>
energia total: 0.319366<br>
X1: -0.495768 Y1: 0.938807<br>
X2: 0.610202 Y2: 0.928932<br>
energia total: 0.32117<br>
X1: -0.495492 Y1: 0.938807<br>
X2: 0.619496 Y2: 0.928932<br>
energia total: 0.32183<br>
X1: -0.495199 Y1: 0.938807<br>
X2: 0.62939 Y2: 0.928932<br>
energia total: 0.323957<br>
X1: -0.494887 Y1: 0.938807<br>
X2: 0.639927 Y2: 0.928932<br>
energia total: 0.324969<br>
X1: -0.494557 Y1: 0.938807<br>
X2: 0.651144 Y2: 0.928932<br>
energia total: 0.327509<br>
X1: -0.494205 Y1: 0.938807<br>
X2: 0.663086 Y2: 0.928932<br>
energia total: 0.328972<br>
X1: -0.493831 Y1: 0.938807<br>
X2: 0.675799 Y2: 0.928932<br>
energia total: 0.332044<br>
X1: -0.493431 Y1: 0.938807<br>
X2: 0.689337 Y2: 0.928932<br>
energia total: 0.334089<br>
X1: -0.493006 Y1: 0.938807<br>
X2: 0.703752 Y2: 0.928932<br>
energia total: 0.337848<br>
X1: -0.49255 Y1: 0.938807<br>
X2: 0.719106 Y2: 0.928932<br>
energia total: 0.34065<br>
X1: -0.492065 Y1: 0.938807<br>
X2: 0.735462 Y2: 0.928932<br>
energia total: 0.345302<br>
X1: -0.491544 Y1: 0.938807<br>
X2: 0.752896 Y2: 0.928932<br>
energia total: 0.349093<br>
X1: -0.490987 Y1: 0.938807<br>
X2: 0.77148 Y2: 0.928932<br>
energia total: 0.354913<br>
X1: -0.490387 Y1: 0.938807<br>
X2: 0.791305 Y2: 0.928932<br>
energia total: 0.360005<br>
X1: -0.489744 Y1: 0.938807<br>
X2: 0.812461 Y2: 0.928932<br>
energia total: 0.367371<br>
X1: -0.489049 Y1: 0.938807<br>
X2: 0.835057 Y2: 0.928932<br>
energia total: 0.374196<br>
X1: -0.488301 Y1: 0.938807<br>
X2: 0.859202 Y2: 0.928932<br>
energia total: 0.383629<br>
X1: -0.48749 Y1: 0.938807<br>
X2: 0.885034 Y2: 0.928932<br>
energia total: 0.39279<br>
X1: -0.486611 Y1: 0.938807<br>
X2: 0.912689 Y2: 0.928932<br>
energia total: 0.40503<br>
X1: -0.485654 Y1: 0.938807<br>
X2: 0.942339 Y2: 0.928932<br>
energia total: 0.417397<br>
X1: -0.484611 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.424542<br>
X1: -0.483466 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.423653<br>
X1: -0.482197 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.431581<br>
X1: -0.480995 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.431328<br>
X1: -0.479706 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.439879<br>
X1: -0.478483 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.440294<br>
X1: -0.47717 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.449523<br>
X1: -0.475922 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.450649<br>
X1: -0.47458 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.460617<br>
X1: -0.473303 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.462507<br>
X1: -0.471929 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.473284<br>
X1: -0.470619 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.476001<br>
X1: -0.469207 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.487673<br>
X1: -0.467859 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.491294<br>
X1: -0.466405 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.503956<br>
X1: -0.465015 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.508573<br>
X1: -0.463512 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.522338<br>
X1: -0.462073 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.528062<br>
X1: -0.460516 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.543063<br>
X1: -0.459023 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.550027<br>
X1: -0.457405 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.566416<br>
X1: -0.45585 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.57478<br>
X1: -0.454163 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.592741<br>
X1: -0.452538 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.602694<br>
X1: -0.450773 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.622442<br>
X1: -0.449071 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.634216<br>
X1: -0.447218 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.656006<br>
X1: -0.445427 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.669881<br>
X1: -0.443475 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.69402<br>
X1: -0.441584 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.710336<br>
X1: -0.43952 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.737191<br>
X1: -0.437515 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.756365<br>
X1: -0.435323 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.786384<br>
X1: -0.433189 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.80893<br>
X1: -0.430852 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.842659<br>
X1: -0.428571 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.869216<br>
X1: -0.426068 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.907326<br>
X1: -0.423618 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.938694<br>
X1: -0.420924 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 0.982022<br>
X1: -0.418279 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.01921<br>
X1: -0.415364 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.06881<br>
X1: -0.412493 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.11312<br>
X1: -0.409322 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.17031<br>
X1: -0.406189 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.22341<br>
X1: -0.402718 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.28992<br>
X1: -0.399276 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.354<br>
X1: -0.395452 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.43207<br>
X1: -0.391644 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932<br>
energia total: 1.51003<br>
X1: -0.387401 Y1: 0.938807<br>
X2: 0.97416 Y2: 0.928932 </p>

<p> (resultado "default", sendo valor da massa 1, massa 2 e energia acumulada, respectivamente, com os valores prontos do código, sendo eles dt = 0.1/30 e loop até 100).</p>


<p>Foi printado a energia total acumulada e as coordenadas x1, y1, x2 e y2, respectivamente, do curso das massas na ponta da mola, sendo os valores iniciais próximo a (-0.5,0) e (+0.5,0), onde se encontra a origem das massas.<br>










<h2><u> 3. Classe Springmass </h2></u>

<h2>3.1 Introdução</h2>

<p>Relatório referente a terceira parte do laboratório de Técnicas de programação 1, orientado pelo professor Teófilo de Campos<br>
<strong>Objetivo:</strong> Se familiarizar com a programação orientada a objeto, praticar conceitos relacionados a classes, atributos, métodos, abstração, escapsulamento, herança e polimorfismo; e<br>
Criar funções que auxiliam na abstração do percurso de uma mola e suas massas, onde essa possui duas massas nas suas pontas. O programa retorna coordenadas que foram tratadas por softwares matemáticos afim de avaliar seu comportamento. </p>

<h2>3.2 Requisitos</h2>

<p><strong>Pacotes:</strong><br>
<strong>Bibliotecas:</strong><br>
    <em>simulation:</em> Biblioteca que contém a classe abstrata simulation;<br>
    <em>iostream:</em> Biblioteca que auxilia nos prints;<br>
    <em>springmass:</em> Biblioteca que contém a classe mola e massa.<br>
<strong>Compiladores:</strong> g++ (GCC) 2015 Free Software Foundation, Inc.<br>
<strong>Versão C++:</strong> 5.3.0</p>

<h2>3.3 Linha de comando</h2>

<p>As linhas usadas para compilar os arquivos foram:<br>
<blockquote>
<pre><code><p> cd < PASTA DO ARQUIVO > <br>
g++ springmass.cpp test-springmass.cpp </p></pre></code>
</blockquote>

<h2>3.4 Arquivos</h2>

<p><strong>springmass.h:</strong>Arquivo cabeçalho. Aqui se encontra todos os atributos e todos os métodos que da classe spring que serão utilizados quando forem criados os objetos<br>
    <em>Atributos da mola</em>: Massa adicionadas as duas pontas, sua força, energia aplicada, rigidez, amortecimento e comprimento; <br>
    <em>Métodos da mola: Step: Calcula o passo da mola. Display: Imprime os valores na tela.</em><br>
<strong>springmass.cpp:</strong> Arquivo de código. Aqui fica as implementações dos métodos citados no arquivo cabeçalho<br>
<strong>test-springmass.cpp:</strong> Aqui é criado o objeto, utilizando a classe criada springmass e chamadas de métodos visando descobrir onde se encontra a mola e suas massas</p>

<h2>3.5 Diagrama das classes</h2>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/classdiagram2.png">

<h2>3.6 Saídas Geradas</h2>

<p><strong>Coordenadas:</strong> <br>
(-0.499722,0.0991)(0.499722,0.0991)$0.999444<br>
(-0.499077,0.0964)(0.499077,0.0964)$0.998154<br>
(-0.49832,0.0919)(0.49832,0.0919)$0.99664<br>
(-0.497541,0.0856)(0.497541,0.0856)$0.995082<br>
(-0.496772,0.0775)(0.496772,0.0775)$0.993543<br>
(-0.496023,0.0676)(0.496023,0.0676)$0.992046<br>
(-0.495298,0.0559)(0.495298,0.0559)$0.990596<br>
(-0.494597,0.0424)(0.494597,0.0424)$0.989194<br>
(-0.49392,0.0271)(0.49392,0.0271)$0.98784<br>
(-0.493266,0.01)(0.493266,0.01)$0.986533<br>
(-0.492635,-0.0089)(0.492635,-0.0089)$0.985271<br>
(-0.492026,-0.0296)(0.492026,-0.0296)$0.984052<br>
(-0.491438,-0.0521)(0.491438,-0.0521)$0.982876<br>
(-0.49087,-0.0764)(0.49087,-0.0764)$0.98174<br>
(-0.490322,-0.1025)(0.490322,-0.1025)$0.980644<br>
(-0.489792,-0.1304)(0.489792,-0.1304)$0.979585<br>
(-0.489281,-0.1601)(0.489281,-0.1601)$0.978563<br>
(-0.488788,-0.1916)(0.488788,-0.1916)$0.977576<br>
(-0.488312,-0.2249)(0.488312,-0.2249)$0.976623<br>
(-0.487852,-0.26)(0.487852,-0.26)$0.975704<br>
(-0.487408,-0.2969)(0.487408,-0.2969)$0.974816<br>
(-0.486979,-0.3356)(0.486979,-0.3356)$0.973958<br>
(-0.486565,-0.3761)(0.486565,-0.3761)$0.973131<br>
(-0.486166,-0.4184)(0.486166,-0.4184)$0.972331<br>
(-0.48578,-0.4625)(0.48578,-0.4625)$0.97156<br>
(-0.485408,-0.5084)(0.485408,-0.5084)$0.970815<br>
(-0.485048,-0.5561)(0.485048,-0.5561)$0.970096<br>
(-0.484701,-0.6056)(0.484701,-0.6056)$0.969402<br>
(-0.484366,-0.6569)(0.484366,-0.6569)$0.968731<br>
(-0.484042,-0.71)(0.484042,-0.71)$0.968084<br>
(-0.48373,-0.7649)(0.48373,-0.7649)$0.967459<br>
(-0.483428,-0.8216)(0.483428,-0.8216)$0.966856<br>
(-0.483137,-0.8801)(0.483137,-0.8801)$0.966274<br>
(-0.482856,-0.9404)(0.482856,-0.9404)$0.965712<br>
(-0.482584,-0.9404)(0.482584,-0.9404)$0.965169<br>
(-0.482322,-0.8801)(0.482322,-0.8801)$0.964645<br>
(-0.482069,-0.8216)(0.482069,-0.8216)$0.964139<br>
(-0.481825,-0.7649)(0.481825,-0.7649)$0.96365<br>
(-0.481589,-0.71)(0.481589,-0.71)$0.963179<br>
(-0.481362,-0.6569)(0.481362,-0.6569)$0.962724<br>
(-0.481142,-0.6056)(0.481142,-0.6056)$0.962284<br>
(-0.48093,-0.5561)(0.48093,-0.5561)$0.96186<br>
(-0.480725,-0.5084)(0.480725,-0.5084)$0.96145<br>
(-0.480527,-0.4625)(0.480527,-0.4625)$0.961054<br>
(-0.480336,-0.4184)(0.480336,-0.4184)$0.960672<br>
(-0.480152,-0.3761)(0.480152,-0.3761)$0.960304<br>
(-0.479974,-0.3356)(0.479974,-0.3356)$0.959948<br>
(-0.479802,-0.2969)(0.479802,-0.2969)$0.959604<br>
(-0.479636,-0.26)(0.479636,-0.26)$0.959272<br>
(-0.479476,-0.2249)(0.479476,-0.2249)$0.958952<br>
(-0.479321,-0.1916)(0.479321,-0.1916)$0.958643<br>
(-0.479172,-0.1601)(0.479172,-0.1601)$0.958344<br>
(-0.479028,-0.1304)(0.479028,-0.1304)$0.958056<br>
(-0.478889,-0.1025)(0.478889,-0.1025)$0.957777<br>
(-0.478754,-0.0764)(0.478754,-0.0764)$0.957509<br>
(-0.478625,-0.0521)(0.478625,-0.0521)$0.957249<br>
(-0.478499,-0.0296)(0.478499,-0.0296)$0.956999<br>
(-0.478379,-0.0089)(0.478379,-0.0089)$0.956757<br>
(-0.478262,0.01)(0.478262,0.01)$0.956524<br>
(-0.478149,0.0271)(0.478149,0.0271)$0.956298<br>
(-0.47804,0.0424)(0.47804,0.0424)$0.956081<br>
(-0.477935,0.0559)(0.477935,0.0559)$0.955871<br>
(-0.477834,0.0676)(0.477834,0.0676)$0.955668<br>
(-0.477736,0.0775)(0.477736,0.0775)$0.955472<br>
(-0.477641,0.0856)(0.477641,0.0856)$0.955283<br>
(-0.47755,0.0919)(0.47755,0.0919)$0.9551<br>
(-0.477462,0.0964)(0.477462,0.0964)$0.954924<br>
(-0.477377,0.0991)(0.477377,0.0991)$0.954754<br>
(-0.477295,0.1)(0.477295,0.1)$0.95459<br>
(-0.477216,0.0991)(0.477216,0.0991)$0.954431<br>
(-0.477139,0.0964)(0.477139,0.0964)$0.954278<br>
(-0.477065,0.0919)(0.477065,0.0919)$0.95413<br>
(-0.476994,0.0856)(0.476994,0.0856)$0.953988<br>
(-0.476925,0.0775)(0.476925,0.0775)$0.95385<br>
(-0.476858,0.0676)(0.476858,0.0676)$0.953717<br>
(-0.476794,0.0559)(0.476794,0.0559)$0.953589<br>
(-0.476732,0.0424)(0.476732,0.0424)$0.953465<br>
(-0.476672,0.0271)(0.476672,0.0271)$0.953345<br>
(-0.476615,0.01)(0.476615,0.01)$0.953229<br>
(-0.476559,-0.0089)(0.476559,-0.0089)$0.953118<br>
(-0.476505,-0.0296)(0.476505,-0.0296)$0.95301<br>
(-0.476453,-0.0521)(0.476453,-0.0521)$0.952906<br>
(-0.476403,-0.0764)(0.476403,-0.0764)$0.952806<br>
(-0.476354,-0.1025)(0.476354,-0.1025)$0.952709<br>
(-0.476308,-0.1304)(0.476308,-0.1304)$0.952615<br>
(-0.476262,-0.1601)(0.476262,-0.1601)$0.952525<br>
(-0.476219,-0.1916)(0.476219,-0.1916)$0.952438<br>
(-0.476177,-0.2249)(0.476177,-0.2249)$0.952353<br>
(-0.476136,-0.26)(0.476136,-0.26)$0.952272<br>
(-0.476097,-0.2969)(0.476097,-0.2969)$0.952194<br>
(-0.476059,-0.3356)(0.476059,-0.3356)$0.952118<br>
(-0.476022,-0.3761)(0.476022,-0.3761)$0.952045<br>
(-0.475987,-0.4184)(0.475987,-0.4184)$0.951974<br>
(-0.475953,-0.4625)(0.475953,-0.4625)$0.951906<br>
(-0.47592,-0.5084)(0.47592,-0.5084)$0.95184<br>
(-0.475888,-0.5561)(0.475888,-0.5561)$0.951776<br>
(-0.475857,-0.6056)(0.475857,-0.6056)$0.951715<br>
(-0.475828,-0.6569)(0.475828,-0.6569)$0.951656<br>
(-0.475799,-0.71)(0.475799,-0.71)$0.951599<br>
(-0.475772,-0.7649)(0.475772,-0.7649)$0.951543<br>

<p> (resultado "default", sendo valor da massa 1, massa 2 e energia acumulada, respectivamente, com os valores prontos do código, sendo eles dt = 0.1/30 e loop até 100).</p>


<p>Foi printado a energia total acumulada e as coordenadas x1, y1, x2 e y2, respectivamente, do curso das massas na ponta da mola, sendo os valores iniciais próximo a (-0.5,0) e (+0.5,0), onde se encontra a origem das massas.<br>

<strong>Gráfico:</strong><br>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/springmain.png" alt="Gráfico gerado no software Octave, valores das variáveis informados na imagem." />
<p>Os resultados obtidos são as massas nas pontas da mola no par de eixos para avaliar com mais eficácia o trajeto da mola.<br>
*Gráfico gerado no software Octave.</p>











<h2><u> 4. Grafics </h2></u>

<h2>4.1 Introdução</h2>

<p>Relatório referente a quarta parte do laboratório de Técnicas de programação 1, orientado pelo professor Teófilo de Campos<br>
<strong>Objetivo:</strong> Se familiarizar com a programação orientada a objeto, praticar conceitos relacionados a classes, atributos, métodos, abstração, escapsulamento, herança e polimorfismo; e<br>
Criar funções que auxiliam na criação da parte gráfica da bola e da mola. O programa retorna uma caixa onde apresenta em tempo real e em gráficos dinâmicos afim de avaliar seu comportamento. </p>

<h2>4.2 Requisitos</h2>

<p><strong>Pacotes:</strong><br>
<strong>Bibliotecas:</strong><br>
    <em>simulation:</em> Biblioteca que contém a classe abstrata simulation;<br>
    <em>iostream:</em> Biblioteca que auxilia nos prints;<br>
    <em>ball:</em> Biblioteca que contém a classe bola;<br>
    <em>springmass:</em> Biblioteca que contém a classe mola e massa.<br>
<strong>Compiladores:</strong> g++ (GCC) 2015 Free Software Foundation, Inc.<br>
<strong>Versão C++:</strong> 5.3.0</p>

<h2>4.3 Linha de comando</h2>

<p>As linhas usadas para compilar os arquivos foram:<br>
<blockquote>
<pre><code><p> cd < PASTA DO ARQUIVO > <br>
g++ graphics.cpp ball.cpp test-ball-graphics.cpp -lglut -lGL -lGLU <br> 
ou<br>
graphics.cpp springmass.cpp test-springmass-graphics.cpp -lglut -lGL -lGLU </p></pre></code>
</blockquote>

<h2>4.4 Arquivos</h2>

<p><strong>test-ball-graphics.cpp;</strong><br>
<strong>test-springmass-graphics.cpp.</strong></p>


<h2>4.5 Diagrama de Classe</h2>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/classdiagramgraphic.png">

<h2>4.6 Saídas Geradas</h2>

<strong>Gráfico Ball:</strong><br>

<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/graficsb1.png" alt="Gráfico gerado no software Octave, bola 1." /><br><br>
<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/graficsb2.png" alt="Gráfico gerado no software Octave, bola 2." /><br><br>
<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/graficsbm1.png" alt="Gráfico gerado no software Octave, bolamola 1." /><br><br>
<img src="https://gitlab.com/ahmoura/lab1_tp1/raw/master/graficsbm2.png" alt="Gráfico gerado no software Octave, bolamola 2." /><br><br>
<p>O resultado é uma imagem da tela do gráfico dinâmico da bola<br>
*Gráfico gerado no software Octave.</p>
