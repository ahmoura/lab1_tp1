/** file: springmass.cpp
 ** brief: SpringMass simulation implementation
 ** author: Antonio Henrique
 **/

#include "springmass.h"
#include <iostream>
using namespace std;

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

Mass::Mass() // Construtor da massa
: position(), velocity(), force(), mass(1), radius(1)
{ }

Mass::Mass(Vector2 position, Vector2 velocity, double mass, double radius)
: position(position), velocity(velocity), force(), mass(mass), radius(radius),
xmin(-1),xmax(1),ymin(-1),ymax(1)
{ }

void Mass::setForce(Vector2 f) // Setar forca
{
  force = f ;
}

void Mass::addForce(Vector2 f) // adicionar forca
{
  force = force + f ;
}

Vector2 Mass::getForce() const // Getar forca
{
  return force ;
}

Vector2 Mass::getPosition() const // Getar posicao
{
  return position ;
}

Vector2 Mass::getVelocity() const // Getar velocidade
{
  return velocity ;
}

double Mass::getRadius() const // Getar Raio da massa
{
  return radius ;
}

double Mass::getMass() const // Getar Masa
{
  return mass ;
}

double Mass::getEnergy(double gravity) const // Atualizar e getar energia acumulada
{
    double energy = 0 ;
	
  double kinetic = 0.5 * mass * velocity.norm2();
  double potential = gravity * mass * (position.y - ymin - radius) ;
  energy = kinetic + potential ;

  return energy;
  
  /* ANOTACAO PARA OUTROS TESTES
  double energy = 0 ;
  double ecin, epot; // Energia potencial e energia cinetica
  double h;
  
  if(this -> getPosition().y > 0){ // correcao do calculo a borda da bola ate o chao
	  h = 1 + (this -> getPosition().y - radius);
  }
  else{
	  h = 1 - (this -> getPosition().y - radius);
  }
  
  epot = (this -> mass) * gravity * h;
  ecin = ((this -> mass) * (this -> velocity.norm2()))/2;
  
  energy = ecin + epot; */
  
}

void Mass::step(double dt, double gravity) // Atualiza os passos de cada elemento que se move na tela
{
	
  double x, y, velX, velY;
  Spring spring(Mass* mass1,Mass* mass2,double naturalLength,double stiffness,const double damping = 0.01);
  Vector2 aceleracao;

  aceleracao.y = (this -> getForce().y)/getMass(); // calcula aceleracao de y, depois atualiza os vetores e velocidade
  y = this -> position.y + this -> velocity.y*dt + ((aceleracao.y - 50*gravity )*(dt*dt))/2;
  velY = this -> getVelocity().y +(aceleracao.y * dt);


  if((this -> ymin + this -> getRadius() <= y) && (this -> ymax - this -> getRadius() >= y)){ // Verifica os valores da caixa para y
    this -> position.y = y;
    this -> velocity.y = velY;
  }

  else{
    this -> velocity.y = -velY + 1;
  }

  aceleracao.x = (this -> getForce().x * 0.015)/ getMass(); // a mesma coisa, calcula aceleracao de x, verifica os vetores e velocidade
  x = this -> position.x + this -> velocity.x*dt + (aceleracao.x * (dt*dt))/2;
  velX = ((this -> getVelocity().x + (aceleracao.x * dt)));


  if((this -> xmin + this -> getRadius() <= x) && (this -> xmax - this -> getRadius() >= x)) {  // testa borda da caixa
    this -> position.x = x;
    this -> velocity.x = velX;
  }
  else{ 
    this -> velocity.x = -velX;
  }



  /* ANOTACOES DE SOLUCOES FALHAS PARA TESTES FUTUROS
  Vector2 accel = force / mass ;
  Vector2 position_ = position + dt * velocity + 0.5 * accel * (dt * dt) ;

  if (xmin + radius <= position_.x &&
    position_.x <= xmax - radius) {
    position.x = position_.x ;
    velocity.x = velocity.x + dt * accel.x ;
  } else {
    velocity.x = - velocity.x ;
  }

  if (ymin + radius <= position_.y &&
      position_.y <= ymax - radius) {
    position.y = position_.y ;
    velocity.y = velocity.y + dt * accel.y ;
  } else {
    velocity.y = - velocity.y ;
  }	
	*/
	
  /*double x, y, veloX, veloY;
  Spring spring(Mass* mass1, Mass* mass2, double naturalLength, double stiffness, const double damping = 0.01);
  Vector2 ac;
  Vector2 position_ = position + dt * velocity + 0.5 * ac * (dt * dt) ;

*/

}
/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

Spring::Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping) // Construtor da Mola
: mass1(mass1), mass2(mass2), naturalLength(naturalLength), stiffness(stiffness), damping(damping)
{ }

Mass * Spring::getMass1() const // Getar massa1
{
  return mass1 ;
}

Mass * Spring::getMass2() const // Getar massa2
{
  return mass2 ;
}

Vector2 Spring::getForce() const // Calcula e retorna forca
{
  Vector2 F ;
  double length;
  double elongationVelocity ;
  double force;
   
  // forca de x (massa)
  Vector2 v1 = mass2->getVelocity() ;
  Vector2 v2 = mass1->getVelocity() ;
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  length = u.norm() ;
  u = u / u.norm() ;
  elongationVelocity = dot(v2 - v1, u) ;
  force = (naturalLength - length) * stiffness + elongationVelocity * damping ;
  F = force * u ;

  return F ;

}

double Spring::getLength() const // Atua liza e retorna lenght
{
  Vector2 u = mass2->getPosition() - mass1->getPosition() ;
  return u.norm() ;
}

double Spring::getEnergy() const { // Atualiza e retorna energia
  double length = getLength() ;
  double dl = length - naturalLength;
  return 0.5 * stiffness * dl * dl ;
}

std::ostream& operator << (std::ostream& os, const Mass& m)
{
  os<<"("
  <<m.getPosition().x<<","
  <<m.getPosition().y<<")" ;
  return os ;
}

std::ostream& operator << (std::ostream& os, const Spring& s)
{
  return os<<"$"<<s.getLength() ;
}

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

SpringMass::SpringMass(double gravity) // Construtor da classe SpringMass
: gravity(gravity)
{ }

void SpringMass::display()
{
  for (int i = 0 ; i < (int)vecmass.size() ; ++i) {
    std::cout<<vecmass[i] ;
  }
  for (int i = 0 ; i < (int)vecsprings.size() ; ++i) {
    std::cout<<vecsprings[i] ;
  }
  std::cout<<" en:"<<getEnergy() ;
  std::cout<<std::endl ;
	

	/* PRINTS ALTERNATIVAS
	cout <<  this->mass1->getPosition().x << " " << mass1->getPosition().y << endl;
	cout <<  this->mass2->getPosition().x << " " << mass2->getPosition().y << endl;
	cout << "energia total: " << this->getEnergy() << "<br>" << endl;
	cout << "X1: " << this->mass1->getPosition().x << " Y1: " << mass1->getPosition().y << "<br>" <<endl;
	cout << "X2: " << this->mass2->getPosition().x << " Y2: " << mass2->getPosition().y  << "<br>"<< endl;	
	cout << "energia total: " << this->getEnergy() << "<br>" << endl;
	cout << "X1: " << this->mass1->getPosition().x << " Y1: " << mass1->getPosition().y <<endl;
	cout << "X2: " << this->mass2->getPosition().x << " Y2: " << mass2->getPosition().y << endl;
	 << "<br>"
	*/
};

double SpringMass::getEnergy() const // Atualizar energia e retornar
{
  double energy = 0 ;

  for (int i = 0 ; i < (int)vecmass.size() ; ++i) {
    energy += vecmass[i].getEnergy(gravity) ;
  }
  for (int i = 0 ; i < (int)vecsprings.size() ; ++i) {
    energy += vecsprings[i].getEnergy() ;
  }
  
  //energy = mass1->getEnergy(gravity) + mass2->getEnergy(gravity) + spring->getEnergy();

  return energy ;
}

void SpringMass::step(double dt) // Atualiza passos das massas da mola
{
  Vector2 g(0,-gravity) ; // Vetor da gravidade, i.e.,
  // aceleracao apontando para baixo.
  
  double energy = 0;
  energy = this->getEnergy();

  for (int i = 0 ; i < (int)vecmass.size()/ 2 ; ++i) { // Atualiza as massas para o vetor massa
    mass1 = &(this->vecmass[2*i]);
    mass2 = &(this->vecmass[2*i+1]);
  }
  for (int i = 0 ; i < (int)vecsprings.size() ; ++i) { //Atualiza forca e joga as massas dentro da funcao
    spring = &(vecsprings[this->vecsprings.size()-1]);
    mass1->setForce(spring -> getForce());
    mass2->setForce(-1.0*spring->getForce());
  }
  for (int i = 0 ; i < (int)vecmass.size() ; ++i) { //Atualiza passos das massas
        mass1->step(dt,gravity);
        mass2->step(dt,gravity);

  }
  /* ANOTACOES PARA TESTE FUTUROS
  energy = this -> getEnergy();
  mass1 -> step(dt, gravity);
  mass2 -> step(dt, gravity);
  mass1 -> setForce(spring -> getForce() * energy);
  mass2 -> setForce(spring -> getForce() * energy);
  
  /*
    mass1 -> setForce(g * mass1-> getMass());
  mass2 -> setForce(g * mass2-> getMass());
  spring -> getMass1()->addForce(-1 * spring -> getForce());
  spring -> getMass2()->addForce(1 * spring -> getForce());
  int i;
  for(i = 0; i > ##sizeof(vmass); i++){ // Atualizar forca
	vmass[i].setForce(gravity * mvmass[i].getMass());
  }
  mass1 -> setForce(g * mass1-> getMass());
  mass2 -> setForce(g * mass2-> getMass());
  spring -> getMass1()->addForce(-1 * spring -> getForce());
  spring -> getMass2()->addForce(1 * spring -> getForce());
  */
  
  /* INCOMPLETE: TYPE YOUR CODE HERE 
     1. para cada massa,
        atualize sua forca usando m*g, i.e.
	masses[i].setForce(g * masses[i].getMass()) 
     2. para cada mola, obtenha a forca da mola e 
        adicione-a a cada massa ligada a esta mola, i.e.,
	springs[i].getMass1()->addForce(-1 * force) ;
	springs[i].getMass2()->addForce(+1 * force) ;     
     3. Atualize a posicao e velocidade de todas as massas,
        i.e., execute o metodo step() delas.
   */
}


int SpringMass::mkMass(Mass vecamass) { // Criar massa
	vecmass.push_back(vecamass);
	return (int)vecmass.size() - 1;
}

double Mass::size(){ // Gerar tamanho da bola
	return radius*radius* 3.14;
}

void SpringMass::mkSpring(int r, int s, double naturalLength, double stiffness) { // Cria mola 
	Spring spring_1(&vecmass[r], &vecmass[s], naturalLength, stiffness);
	vecsprings.push_back(spring_1);
	
};
