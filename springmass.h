/** file: springmass.h
 ** brief: SpringMass simulation
 ** author: Andrea Vedaldi
 **/

#ifndef __springmass__
#define __springmass__

#include "simulation.h"

#include <cmath>
#include <vector>

#define MOON_GRAVITY 1.62
#define EARTH_GRAVITY 9.82

/* ---------------------------------------------------------------- */
// class Vector2
/* ---------------------------------------------------------------- */

class Vector2 // classe com as coordenadas dos objetos
{
public:
  double x ;
  double y ;

  Vector2() : x(0), y(0) { }
  Vector2(double _x, double _y) : x(_x), y(_y) { }
  double norm2() const { return x*x + y*y ; }
  double norm() const { return std::sqrt(norm2()) ; }
} ;

inline Vector2 operator+ (Vector2 a, Vector2 b) { return Vector2(a.x+b.x, a.y+b.y) ; }
inline Vector2 operator- (Vector2 a, Vector2 b) { return Vector2(a.x-b.x, a.y-b.y) ; }
inline Vector2 operator* (double a, Vector2 b)  { return Vector2(a*b.x, a*b.y) ; }
inline Vector2 operator* (Vector2 a, double b)  { return Vector2(a.x*b, a.y*b) ; }
inline Vector2 operator/ (Vector2 a, double b)  { return Vector2(a.x/b, a.y/b) ; }
inline double dot(Vector2 a, Vector2 b) { return a.x*b.x + a.y*b.y ; }

/* ---------------------------------------------------------------- */
// class Mass
/* ---------------------------------------------------------------- */

class Mass // classe com as informacoes das massas
{
public:
  Mass() ;
  Mass(Vector2 position, Vector2 velocity, double mass, double radius) ;
  void setForce(Vector2 f) ;
  void addForce(Vector2 f) ;
  Vector2 getForce() const ;
  Vector2 getPosition() const ;
  Vector2 getVelocity() const ;
  double getMass() const ;
  double getRadius() const ;
  double getEnergy(double gravity) const ;
  void step(double dt, double gravity) ;
  double size();
  
protected:
  Vector2 force ;
  Vector2 position ; 
  Vector2 velocity ;
  double mass;
  double radius ; // Assume-se massas de forma esfericas.
  double energy(double gravity) ; // Energia aplicada a bola.
  
  double xmin ; // Limites da caixa onde o sistema funcionara,
  double xmax ; // x horizontal e y vertical
  double ymin ;
  double ymax ;
} ;

/* ---------------------------------------------------------------- */
// class Spring
/* ---------------------------------------------------------------- */

class Spring // classe com as informacoes da mola
{
public:
  Spring(Mass * mass1, Mass * mass2, double naturalLength, double stiffness, double damping = 0.01) ;
  Mass * getMass1() const ;
  Mass * getMass2() const ;
  Vector2 getForce() const ;
  double getLength() const ;
  double getEnergy() const ;

protected:

   Mass *mass1 ;
   Mass *mass2 ;
   Vector2 force ;
   double stiffness;
   double naturalLength;
   double damping;
   double lenght ;
   double energy ;
} ;

/* ---------------------------------------------------------------- */
// class SpringMass : public Simulation
/* ---------------------------------------------------------------- */

class SpringMass : public Simulation // classe que relaciona a mola com as massas pelo vetor
{
public:
  SpringMass(double gravity = MOON_GRAVITY);
  void step(double dt) ;
  void display() ;
  double getEnergy() const ;

  int mkMass(Mass vecmass); // cria massa e instancia com construtor default;
  void mkSpring(int r, int s, double naturalLength, double stiffness); // cria mola e instancia com construtor default;
  

protected:
  double gravity ;
  Mass *mass1;
  Mass *mass2;
  Spring *spring;
  double energy;
  
  
    typedef std::vector<Mass> mass_t; // definido nome para o vetor de massas.
    typedef std::vector<Spring> spring_t; // definido nome o vetor de molas.
	mass_t vecmass; // declarando vetor de massas.
	spring_t vecsprings; // declarando vetor de molas.
	
} ;

#endif /* defined(__springmass__) */

