/** file: ball.h
 ** brief: Ball class
 ** author: Andrea Vedaldi
 **/

#ifndef __ball__
#define __ball__

#include "simulation.h"

class Ball : public Simulation
{
public:
// Constructors and member functions
 // Ball(double x, double y) ;
  Ball();
  void step(double dt) ;
  void display() ;
  void run(double dt);

  double setX(double aux) {	//Fun��o para alterar a vari�vel x;
	  this->x = aux;
  }
  double getX(){	//Fun��o para obter a vari�vel x;
	  return this->x;
  }
  double setY(double aux) { //Fun��o para alterar a vari�vel y;
	  this->x = aux;
  }
  double getY() { //Fun��o para obter a vari�vel y;
	  return this->x;
  }

protected:
  // Data members
  // Position and velocity of the ball
  double x ;
  double y ;
  double vx ;
  double vy ;

  // Mass and size of the ball
  double m ;
  double r ;

  // Gravity acceleration
  double g ;

  // Geometry of the box containing the ball
  double xmin ;
  double xmax ;
  double ymin ;
  double ymax ;
} ;

#endif /* defined(__ball__) */
