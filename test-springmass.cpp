/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Antonio Henrique
 **/

#include "springmass.h"

int main(int argc, char** argv)
{
  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double dt = 1.0/30 ;
  const double stiffness = 0.5;
  const double damping = 0.5;

  Mass m1(Vector2(-.5,0.1), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0.1), Vector2(), mass, radius) ;
  
  springmass.mkMass(m1) ; //joga valores da massa 1 no vetor;
  springmass.mkMass(m2) ; //joga valores da massa 2 no vetor;
  springmass.mkSpring(0,1, naturalLength, stiffness, damping) ;//joga valores da mola no vetor;

  for (int i = 0 ; i < 100 ; ++i) { // loop para atualizar  e printar a posicao e a energia 
    springmass.step(dt) ;
    springmass.display() ;
  }

  return 0 ;
}
