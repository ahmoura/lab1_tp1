#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
private:
  Figure figure;

public:
	SpringMassDrawable(): SpringMass(),figure("SpringMass") //Nome da barra e funcao construtura
	{
    figure.addDrawable(this) ;    //método responsável por chamar os construtores das duas massas e a  da mola
  	}

  	void draw() {
    	figure.drawCircle(mass1->getPosition().x, mass1->getPosition().y, mass1->getRadius()); // funcao que desenha a massa 1;
    	figure.drawCircle(mass2->getPosition().x, mass2->getPosition().y, mass2->getRadius()); // funcao que desenha a massa 2;
   		figure.drawLine(mass1->getPosition().x, mass1->getPosition().y, mass2->getPosition().x, mass2->getPosition().y , 0.03); // funcao que desenha a mola
  	}

  	void display() { //funcao que printa os graficos
    	figure.update() ;
  	}

} ;

int main(int argc, char** argv)
{
	glutInit(&argc,argv) ;
	SpringMassDrawable springmass ;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 1;

  Mass mass1(Vector2(-0.5,0), Vector2(), mass, radius);
  Mass mass2(Vector2(0.5,0), Vector2(), mass, radius); 

  springmass.mkMass(mass1); // coloca massa 1 no vetor;
  springmass.mkMass(mass2); // coloca massa 2 no vetor;
  springmass.mkSpring(0, 1, naturalLength, 1); // coloca a mola no vetor
  
  springmass.display(); // printa a tela
  springmass.draw(); // printa os desenhos;
	
  run(&springmass, 1/60.0) ; // corre os desenhos na tela

  return 0;
}
