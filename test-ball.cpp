/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>
using namespace std;

int main(){
	
  Ball ball;
  ball.run(1.0/10000);
  
  return 0;
}
